(function() {
    'use strict';

    angular
    .module('webapp')
    .controller('ResultController', ResultController);

    /** @ngInject */
    function ResultController($timeout, $scope, toastr, $state, $stateParams, TripService, UserService, ApiService) {

        $scope.percent = $stateParams.percent;
        $scope.tripHistory = [];

        var trip = TripService.get();
        trip.rewardspoints = $stateParams.percent * 5;
        ApiService.updateTrip(trip).then(function (){
            TripService.clear();
            ApiService.getTripHistory().then(function(history){
                $scope.tripHistory = history;
            })
        });
    }

})();
