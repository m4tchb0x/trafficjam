(function() {
    'use strict';

    angular
    .module('webapp')
    .controller('LoginController', LoginController);

    function LoginController($timeout, $scope, $state, toastr, UserService) {
        $scope.loginObj = {};

        $timeout(loaderDone, 3000);
        
        $scope.submitLogin = function(){
            UserService.login($scope.loginObj).then(function(data){
                $state.go('home');
            }, function(){
                toastr['error']('Failed to login!');
            });
        };

        function loaderDone() {
            $scope.loaded = true;
        }

    }


})();
    