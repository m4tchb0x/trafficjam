(function() {
    'use strict';

    angular
    .module('webapp')
    .controller('MainController', MainController);

    /** @ngInject */
    function MainController($timeout, $scope, toastr, TomTomService, $state, ApiService, UserService, TripService) {

        $scope.fromPosition = null;
        $scope.fromLocation = null;
        $scope.toPosition = null;
        $scope.toLocation = null;
        $scope.toLocationList = [];

        var typingTimer;                //timer identifier
        var doneTypingInterval = 1000;  //time in ms, 5 second for example

        //on keyup, start the countdown
        $scope.resetType = function (){
            clearTimeout(typingTimer);
            if ($scope.toLocation) {
                typingTimer = setTimeout(doneTyping, doneTypingInterval);
            }
        };

        $scope.routeMe = function (){
            console.log($scope.fromPosition, $scope.fromLocation);

            ApiService.postTrip({
                from_streetnumber: $scope.fromLocation,
                from_streetname: $scope.fromLocation,
                from_city: $scope.fromLocation,
                from_state: $scope.fromLocation,
                from_postalcode: $scope.fromLocation,
                from_placeid: $scope.fromLocation,
                from_latitude: $scope.fromPosition.latitude,
                from_longitude: $scope.fromPosition.longitude,
                to_streetnumber: $scope.toLocation,
                to_streetname: $scope.toLocation,
                to_city: $scope.toLocation,
                to_state: $scope.toLocation,
                to_postalcode: $scope.toLocation,
                to_placeid: $scope.toLocation,
                to_latitude: $scope.toPosition.latitude,
                to_longitude: $scope.toPosition.longitude,
                travelmode: 'driving',
                rewardspoints: 0,
                createdon: new Date(),
                userid: UserService.get().id
            }).then(
                function (trip){
                    TripService.set(trip);
                });

            $state.go('trip', {'from': $scope.fromPosition, 'to': $scope.toPosition});
        };

        $scope.setToLocation = function (location){
            $scope.toPosition = location;
            $scope.toLocation = location.formattedAddress;
            $scope.toLocationList = [];
        };

        //user is "finished typing," do something
        function doneTyping () {
           TomTomService.getPointFromLocation($scope.toLocation).then(
                function (data){
                    $scope.toLocationList = data;
                },
                function(){
                    $scope.toLocationList = [];
                });
        }



        var onSuccess = function(position) {
            $scope.fromPosition = position.coords;

            TomTomService.getLocationFromPoint(position.coords).then(function (data){
                $scope.fromLocation = data;
            });
        };

        // onError Callback receives a PositionError object
        //
        function onError(error) {
            console.log('code: '    + error.code    + '\n' +
                  'message: ' + error.message + '\n');
        }

        $scope.refreshFrom = function (){
            navigator.geolocation.getCurrentPosition(onSuccess, onError);
        };

        $scope.refreshFrom();
    }

})();
