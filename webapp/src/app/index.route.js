(function() {
  'use strict';

  angular
    .module('webapp')
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/home',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })
      .state('login', {
        url: '/login',
        templateUrl: 'app/login/login.html',
        controller: 'LoginController',
        controllerAs: 'login'
      })
      .state('trip', {
        url: '/trip',
        templateUrl: 'app/trip/trip.html',
        controller: 'TripController',
        controllerAs: 'trip',
        params: { from: null, to: null, },
        resolve: {
            path: function ($stateParams, TomTomService){
                return TomTomService.getRoute($stateParams.from, $stateParams.to, false).then(function (data){
                    var path = [];
                    var points = data.route.pathPoints;

                    var offset = {latitude: 0,longitude: 0};
                    for (var index = 0; index < points.latitude.length; index++){
                        offset.latitude += points.latitude[index];
                        offset.longitude += points.longitude[index];
                        path.push(angular.copy(offset));
                    }
                    return path;
                });
            }
        }
      })
      .state('finished',{
        url: '/finished',
        templateUrl: 'app/result/result.html',
        controller: 'ResultController',
        controllerAs: 'result',
        params: { percent: null }
      });

    $urlRouterProvider.otherwise('/login');
  }

})();
