'use strict';
 
var CordovaInit = function() {
    var onDeviceReady = function() {
        receivedEvent('deviceready');

    };
 
    var receivedEvent = function(event) {
        console.log('Start event received, bootstrapping application setup.');
        angular.bootstrap($('body'), ['webapp']);
    };
 
    this.bindEvents = function() {
        document.addEventListener('deviceready', onDeviceReady, false);
        document.addEventListener('DOMContentLoaded', function() {
                FastClick.attach(document.body);
            }, false);
    };
 
    //If cordova is present, wait for it to initialize, otherwise just try to
    //bootstrap the application.
    if (window.cordova !== undefined) {
        //window.location.href = 'http://127.0.0.1/android_asset/www/index.html';
        //alert('cordova');
        console.log('Cordova found, wating for device.');
        this.bindEvents();
    } else {
        console.log('Cordova not found, booting application');
        receivedEvent('manual')
    }
};
 
$(function() {
    console.log('Bootstrapping!');
    new CordovaInit();
});