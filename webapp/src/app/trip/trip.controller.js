(function() {
    'use strict';

    angular
    .module('webapp')
    .controller('TripController', TripController);

    function TripController($timeout, $scope, toastr, $stateParams, $state, TomTomService, path, TripService, ApiService, UserService) {

        //console.log(path);
        $scope.map = { center: { latitude: 45, longitude: -73 }, zoom: 10, control: {}};
        $scope.showMap = false;
        var pathCopy = angular.copy(path);

        //var map = new google.maps.Map(elem[0], map);

        $scope.polys = [
        {
        "id": 1,
        "path": path,
        "stroke": {
          "color": "#6060FB",
          "weight": 3
        },
        "editable": false,
        "draggable": true,
        "geodesic": true,
        "visible": true
        }];

        var onSuccess = function (point){
            //console.log(point.coords);
            updatePath(point.coords);
            $scope.map.control.refresh({latitude: point.coords.latitude, longitude: point.coords.longitude});
            $scope.map.control.getGMap().setZoom(17);
            $scope.showMap = true;

            if(TripService.get()){
                ApiService.postLocationHistory({
                    userid: UserService.get().id,
                    latitude: point.coords.latitude,
                    longitude: point.coords.longitude,
                    timestamp: new Date(),
                    travelhistoryid: TripService.get().id
                });
            }
        };

        var updatePath = function (point){
            for(var index = 0; index < pathCopy.length; index++){
                var pathPoint = pathCopy[index];
                if(pathPoint.touched)
                    continue;
                var distanceBetween = distance(point, pathPoint);
                //console.log(distanceBetween);
                if( distanceBetween < 0.07){
                    pathPoint.touched = true;
                    if(index === pathCopy.length - 1){
                        $scope.finishTrip();
                    }
                }
            }
        };

        var distance = function (point1, point2) {
            var lat1 = point1.latitude;
            var lon1 = point1.longitude;
            var lat2 = point2.latitude;
            var lon2 = point2.longitude;
            var p = 0.017453292519943295;    // Math.PI / 180
            var c = Math.cos;
            var a = 0.5 - c((lat2 - lat1) * p)/2 + 
                    c(lat1 * p) * c(lat2 * p) * 
                    (1 - c((lon2 - lon1) * p))/2;

            return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
        }


        $scope.finishTrip = function (){
            var touchedCount = 0;
            for(var index = 0; index < pathCopy.length; index++){
                var pathPoint = pathCopy[index];
                if(pathPoint.touched)
                    touchedCount++;
            }

            var pathTouchedPercent = touchedCount > 0 ? touchedCount/pathCopy.length : 0;
            console.log(touchedCount, pathCopy.length, pathTouchedPercent);

            $state.go('finished',{percent: pathTouchedPercent});
        };

        var checkLocation = function (){
            navigator.geolocation.getCurrentPosition(onSuccess, undefined);
        };

        var t = setInterval(checkLocation, 2000);
    }

})();
