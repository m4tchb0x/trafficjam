(function() {
  'use strict';

  angular
    .module('webapp')
    .factory('UserService', function ($http, $q) {
        var user = null;

        var get = function(){
            return user;
        };

        var login = function(loginObj){
            var deferred = $q.defer();
            $http.get("https://trafficjam.azure-mobile.net/tables/user?$filter=(username%20eq%20%27"+loginObj.user+"%27%20and%20password%20eq%20%27"+loginObj.pass+"%27)").then(
                    function(result){ //success
                        if(result.data.length > 0){
                            user = result.data[0];
                            deferred.resolve(user);
                        }else{
                            deferred.reject();
                        }
                    },
                    function(){ //error
                        deferred.reject();
                    }
                );
            return deferred.promise;
        };

        var register = function(user){

        };

        return {
            get : get,
            login : login,
            register : register
        };

    });

})();
