(function() {
  'use strict';

  angular
    .module('webapp')
    .factory('ApiService', function ($http, $q, TripService, UserService) {

        var postLocationHistory = function (locationHistory){
            var deferred = $q.defer(); 
            $http.post("https://trafficjam.azure-mobile.net/tables/locationhistory", locationHistory).then(
                    function(result){
                        deferred.resolve(result.data);
                    },
                    function(){
                        deferred.reject();
                    }
                );
            return deferred.promise;
        };

        var postTrip = function ( travelHistory ){
            var deferred = $q.defer(); 
            $http.post("https://trafficjam.azure-mobile.net/tables/travelhistory", travelHistory).then(
                    function(result){
                        TripService.set(result.data);
                        deferred.resolve(result.data);
                    },
                    function(){
                        deferred.reject();
                    }
                );
            return deferred.promise;
        };

        var updateTrip = function ( travelHistory ){
            var deferred = $q.defer();

            var tempReward = {
                travelhistoryid: travelHistory.id,
                userid: UserService.get().id,
                rewardpoints: travelHistory.rewardspoints,
                rewarddate: new Date()
            };

            $http.post("https://trafficjam.azure-mobile.net/tables/travelrewards", tempReward).then(
                    function(result){
                        TripService.clear();
                        deferred.resolve(result.data);
                    },
                    function(){
                        deferred.reject();
                    }
                );
            return deferred.promise;
        };

        var getTripHistory = function ( userId ){
            var deferred = $q.defer(); 
            $http.get("https://trafficjam.azure-mobile.net/tables/travelrewards?$filter=(UserId%20eq%20%27"+UserService.get().id+"%27%20)").then(
                    function(result){
                        deferred.resolve(result.data);
                    },
                    function(){
                        deferred.reject();
                    }
                );
            return deferred.promise;
        };

        return {
            postLocationHistory: postLocationHistory,
            postTrip: postTrip,
            updateTrip: updateTrip,
            getTripHistory: getTripHistory
        };

    });

})();
