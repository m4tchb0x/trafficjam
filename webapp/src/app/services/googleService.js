(function() {
  'use strict';

  angular
    .module('webapp')
    .factory('GoogleService', function ($http, $q) {

        var googleKey = 'mbx89fshv7wqqxsvrwpjxne7';

        var getRoute = function (fromCoords, toCoords, avoidTolls){
            var deferred = $q.defer();
            $http.jsonp('https://api.tomtom.com/lbs/services/route/3/'+fromCoords.latitude+','+fromCoords.longitude+':'+toCoords.latitude+','+toCoords.longitude+'/Quickest/json?key='+tomtomKey+'&avoidTraffic=true&includeTraffic=true&language=en&day=today&time=now&iqRoutes=2&trafficModelID=-1&avoidTolls='+avoidTolls+'&includeInstructions=true&pathPoints=6&callback=JSON_CALLBACK').then(
                    function(result){
                        deferred.resolve(result.data);
                    },
                    function(){
                        deferred.reject();
                    }
                );
            return deferred.promise;
        };

        var getLocationFromPoint = function (point){
            var deferred = $q.defer();
            $http.jsonp('https://api.tomtom.com/lbs/services/reverseGeocode/3/json?key='+tomtomKey+'&point='+point.latitude+','+point.longitude+'&type=Regional&callback=?').then(
                    function(result){
                        console.log(result);
                        deferred.resolve(result.data);
                    },
                    function(){
                        deferred.reject();
                    }
                );
            return deferred.promise;
        };

        return {
            getRoute : getRoute,
            getLocationFromPoint : getLocationFromPoint
        };

    });

})();
