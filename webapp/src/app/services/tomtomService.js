(function() {
  'use strict';

  angular
    .module('webapp')
    .factory('TomTomService', function ($http, $q) {

        var tomtomKey = 'mbx89fshv7wqqxsvrwpjxne7';

        var getRoute = function (fromCoords, toCoords, avoidTolls){
            var deferred = $q.defer();
            $http.get('https://api.tomtom.com/lbs/services/route/3/'+fromCoords.latitude+','+fromCoords.longitude+':'+toCoords.latitude+','+toCoords.longitude+'/Quickest/json?key='+tomtomKey+'&avoidTraffic=true&includeTraffic=true&language=en&day=today&time=now&iqRoutes=2&trafficModelID=-1&avoidTolls='+avoidTolls+'&includeInstructions=true&pathPoints=18').then(
                    function(result){
                        //console.log(result);
                        deferred.resolve(result.data);
                    },
                    function(){
                        deferred.reject();
                    }
                );
            return deferred.promise;
        };

        var getLocationFromPoint = function (point){
            var deferred = $q.defer();
            $http.get('https://api.tomtom.com/lbs/services/reverseGeocode/3/json?key='+tomtomKey+'&point='+point.latitude+','+point.longitude+'&type=Regional').then(
                    function(result){
                        if(result.data.reverseGeoResponse.reverseGeoResult.length > 0){
                            var location = result.data.reverseGeoResponse.reverseGeoResult[0];
                            deferred.resolve(location.street);
                        }
                        deferred.reject();
                    },
                    function(){
                        deferred.reject();
                    }
                );
            return deferred.promise;
        };


        var getPointFromLocation = function (location){
            var deferred = $q.defer();
            $http.get('https://api.tomtom.com/lbs/services/geocode/4/geocode?CN=Canada&query='+ location + '&format=json&key=' + tomtomKey).then(
                    function(result){
                        if(result.data.geoResponse.geoResult.length > 0){
                            deferred.resolve(result.data.geoResponse.geoResult);
                        }
                        deferred.reject();
                    },
                    function(){
                        deferred.reject();
                    }
                );
            return deferred.promise;
        };
        https://api.tomtom.com/lbs/services/geocode/4/geocode?format=json&key=

        return {
            getRoute : getRoute,
            getLocationFromPoint : getLocationFromPoint,
            getPointFromLocation : getPointFromLocation
        };

    });

})();
