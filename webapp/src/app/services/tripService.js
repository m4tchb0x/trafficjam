(function() {
  'use strict';

  angular
    .module('webapp')
    .factory('TripService', function ($http, $q) {

        var trip = null;

        var get = function() {
            return trip;
        }

        var set = function (setTrip){
            trip = setTrip;
        }

        var clear = function (){
            trip = null;
        }

        return {
            get: get,
            set: set,
            clear: clear
        };

    });

})();